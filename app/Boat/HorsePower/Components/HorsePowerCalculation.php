<?php

namespace App\Boat\HorsePower\Components;

class HorsePowerCalculation
{
    /**
     * @var int
     */
    private $hullLength;

    /**
     * @var int
     */
    private $buttockAngle;

    /**
     * @var int
     */
    private $displacement;

    /**
     * HorsePowerCalculation constructor.
     *
     * @param int $hullLength
     * @param int $buttockAngle
     * @param int $displacement
     */
    public function __construct(
        int $hullLength,
        int $buttockAngle,
        int $displacement
    )
    {
        $this->hullLength   = $hullLength;
        $this->buttockAngle = $buttockAngle;
        $this->displacement = $displacement;
    }

    /**
     * @return float
     */
    public function getSLRatio()
    {
        return round(($this->buttockAngle * -0.2) + 2.9, 2);
    }

    /**
     * @return float
     */
    public function getHullSpeed()
    {
        return round($this->getSLRatio() * pow($this->hullLength, 0.5), 2);
    }

    /**
     * @return float
     */
    public function getCw()
    {
        return round(0.8 + (0.17 * $this->getSLRatio()), 2);
    }

    /**
     * @return float
     */
    public function getHorsePower()
    {
        return round(
            pow(
                ($this->displacement / 1000) * ($this->getHullSpeed() / ($this->getCw() * pow($this->hullLength, 0.5))),
                3
            )
            , 2
        );
    }
}