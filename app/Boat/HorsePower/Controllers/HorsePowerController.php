<?php

namespace App\Boat\HorsePower\Controllers;

use App\Boat\HorsePower\Components\HorsePowerCalculation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use View;

class HorsePowerController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make(
            $data, [
            'hull_length'   => ['required', 'numeric', 'min:0'],
            'buttock_angle' => ['required', 'numeric', 'min:2', 'max:7'],
            'displacement'  => ['required', 'numeric', 'min:0'],
        ]
        );
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function displayHorsePower(Request $request)
    {
        $data = $request->all();
        $validator = $this->validator($data)->validate();

        $view = View::make('boat.horsePower.index');
        $view->with('data', $data);

        if(!$validator) {
            $errors = $validator->errors();
            $view->with('errors', $errors);
        } else {
            $horsePowerCalculation = new HorsePowerCalculation($request->hull_length, $request->buttock_angle, $request->displacement);
            $view->with('horsePowerCalculation', $horsePowerCalculation);
        }

        return $view;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function horsePower()
    {
        return View::make('boat.horsePower.index', ['data' => []]);
    }
}
