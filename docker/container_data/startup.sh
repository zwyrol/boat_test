#!/bin/bash

ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/

/etc/init.d/apache2 start

cd /var/www/html
composer install
cp .env.example .env
php artisan key:generate


/bin/bash
