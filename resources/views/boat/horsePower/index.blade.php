<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div>
    <form action="/horse-power" method="post">
        @csrf

        <section>
            <label>Hull Length (HL)</label>
            <input type="number" name="hull_length" value="{{ isset($data['hull_length']) ? $data['hull_length'] : '' }}" />
        </section>

        <section>
            <label>Buttock Angle</label>
            <input type="number" name="buttock_angle" value="{{ isset($data['buttock_angle']) ? $data['buttock_angle'] : '' }}" />
        </section>

        <section>
            <label>Displacement</label>
            <input type="number" name="displacement"  value="{{ isset($data['displacement']) ? $data['displacement'] : '' }}"  />
        </section>

        <section>
            <input type="submit" value="Get Horse Power"/>
        </section>
    </form>

    @if ($errors && $errors->any())
        <div>
            {{ implode("\n", $errors->all(':message')) }}
        </div>
    @endif

    @if (isset($horsePowerCalculation))
        <div>
            <p>SL Ratio: {{$horsePowerCalculation->getSLRatio()}}</p>
            <p>CW: {{$horsePowerCalculation->getCw()}}</p>
            <p>Hull Speed: {{$horsePowerCalculation->getHullSpeed()}}</p>
            <p>Horse Power: {{$horsePowerCalculation->getHorsePower()}}</p>
        </div>
    @endif

</div>
</body>
</html>
