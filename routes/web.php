<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post(
    '/horse-power',
    array('as' => 'boat.getHorsePower', 'uses' => '\App\Boat\HorsePower\Controllers\HorsePowerController@displayHorsePower')
);


Route::get(
    '/horse-power',
    array('as' => 'boat.horsePower', 'uses' => '\App\Boat\HorsePower\Controllers\HorsePowerController@horsePower')
);


//Route::get('/', function () {
//    return view('welcome');
//});
