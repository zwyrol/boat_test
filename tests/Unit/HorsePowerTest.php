<?php

namespace Tests\Unit;

use App\Boat\HorsePower\Components\HorsePowerCalculation;
use Tests\TestCase;

class HorsePowerTest extends TestCase
{
    /**
     * @return void
     */
    public function testHorsePowerGetHorsePower()
    {
        $hullLength = 30;
        $buttockAngle = 3;
        $displacement = 600;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getHorsePower(), 1.56);

        $hullLength = 70;
        $buttockAngle = 6;
        $displacement = 430;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);
        $this->assertEquals($horsePowerCalculation->getHorsePower(), 0.3);
    }


    /**
     * @return void
     */
    public function testHorsePowerGetCw()
    {
        $hullLength = 3;
        $buttockAngle = 3;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getCw(), 1.19);

        $hullLength = 6;
        $buttockAngle = 6;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getCw(), 1.09);
    }

    /**
     * @return void
     */
    public function testHorsePowerGetHullSpeed()
    {
        $hullLength = 3;
        $buttockAngle = 3;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getHullSpeed(), 3.98);

        $hullLength = 6;
        $buttockAngle = 6;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getHullSpeed(), 4.16);
    }

    /**
     * @return void
     */
    public function testHorsePowerGetSlRatio()
    {
        $hullLength = 3;
        $buttockAngle = 3;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getSLRatio(), 2.3);

        $hullLength = 3;
        $buttockAngle = 6;
        $displacement = 60;

        $horsePowerCalculation = new HorsePowerCalculation($hullLength, $buttockAngle, $displacement);

        $this->assertEquals($horsePowerCalculation->getSLRatio(), 1.7);
    }
}
